import Ember from 'ember';

export default Ember.Route.extend({
  afterModel: function(item) {
    Ember.assert('Must use a valid route', item && this.modelFor('folder') === item.get('folder'));
  }
});
