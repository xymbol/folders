import Ember from 'ember';

export default Ember.Route.extend({
  afterModel: function(folder) {
    Ember.assert('Must use a valid route', folder);
  }
});
