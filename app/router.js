import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('folders', function() {
    this.resource('folder', { path: ':folder_id' }, function() {
      this.resource('items', { path: 'items' }, function() {
        this.resource('item', { path: ':item_id' });
      });
    });
  });
});

export default Router;
