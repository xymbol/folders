import DS from 'ember-data';

var Folder = DS.Model.extend({
  items: DS.hasMany('item', { async: true }),
  name: DS.attr('string')
});

Folder.reopenClass({
  FIXTURES: [
    { id: 1, name: 'Folder #1', items: [1, 2, 3] },
    { id: 2, name: 'Folder #2', items: [4, 5, 6] },
    { id: 3, name: 'Folder #3', items: [7, 8, 9] }
  ]
});

export default Folder;
