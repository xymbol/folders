import DS from 'ember-data';

var Item = DS.Model.extend({
  folder: DS.belongsTo('folder'),
  name: DS.attr('name')
});

Item.reopenClass({
  FIXTURES: [
    { id: 1, name: 'Item #1', folderId: 1 },
    { id: 2, name: 'Item #2', folderId: 1 },
    { id: 3, name: 'Item #3', folderId: 1 },
    { id: 4, name: 'Item #4', folderId: 2 },
    { id: 5, name: 'Item #5', folderId: 2 },
    { id: 6, name: 'Item #6', folderId: 2 },
    { id: 7, name: 'Item #7', folderId: 3 },
    { id: 8, name: 'Item #8', folderId: 3 },
    { id: 9, name: 'Item #9', folderId: 3 }
  ]
});

export default Item;
